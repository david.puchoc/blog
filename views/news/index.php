<?php
include(__DIR__ . '/../templates/header.php'); ?>

<div class="container">
    <div class="row">
        <?php foreach ($articles as $article): ?>
            <div class="col-md-6 mb-4">
                <div class="card">
                    <img src="<?php echo $article['urlToImage']; ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $article['title']; ?></h5>
                        <p class="card-text"><?php echo $article['description']; ?></p>
                        <a href="<?php echo $article['url']; ?>" class="btn btn-primary">Leer más</a>
                        <p class="card-text">Nombre y apellido del autor: </p>
                        <p class="card-text autor">Autor</p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center mt-4">
            <li class="page-item <?php echo $page == 1 ? 'disabled' : ''; ?>">
                <a class="page-link" href="?page=<?php echo $page - 1; ?>&pageSize=<?php echo $pageSize; ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                <li class="page-item <?php echo $i == $page ? 'active' : ''; ?>">
                    <a class="page-link" href="?page=<?php echo $i; ?>&pageSize=<?php echo $pageSize; ?>"><?php echo $i; ?></a>
                </li>
            <?php endfor; ?>
            <li class="page-item <?php echo $page == $totalPages ? 'disabled' : ''; ?>">
                <a class="page-link" href="?page=<?php echo $page + 1; ?>&pageSize=<?php echo $pageSize; ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>
</body>

<script>
    $('.autor').each(function() {
        var autorElement = $(this);

        $.ajax({
            url: 'https://randomuser.me/api/',
            dataType: 'json',
            success: function(data) {
                var firstName = data.results[0].name.first;
                var lastName = data.results[0].name.last;

                autorElement.text(firstName + ' ' + lastName);
            }
        });
    });
</script>

<?php include(__DIR__ . '/../templates/footer.php'); ?>