<?php
require_once('../models/NewsModel.php');

class NewsController {
    public function index() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $pageSize = isset($_GET['pageSize']) ? $_GET['pageSize'] : 10;

        $newsModel = new NewsModel();
        $totalResults = $newsModel->getTotalResults();
        $totalPages = ceil($totalResults / $pageSize);
        $articles = $newsModel->getArticles($page, $pageSize);

        include('../views/news/index.php');
    }
}
