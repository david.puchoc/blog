<?php
require_once('../config/config.php');

class NewsModel {
    public function getTotalResults() {
        $url = BASE_URL . ARTICLES_ENDPOINT . 'apiKey=' . API_KEY;
        $response = @file_get_contents($url);
        $data = json_decode($response, true);

        if ($data && isset($data['status']) && $data['status'] == 'ok') {
            return $data['totalResults'];
        } else {
            return 0;
        }
    }

    public function getArticles($page, $pageSize) {
        $startIndex = ($page - 1) * $pageSize;
        $url = BASE_URL . ARTICLES_ENDPOINT . "page=$page&pageSize=$pageSize" . '&apiKey=' . API_KEY ;
        $response = @file_get_contents($url);
        $data = json_decode($response, true);

        if ($data && isset($data['status']) && $data['status'] == 'ok') {
            return $data['articles'];
        } else {
            return [];
        }
    }
}
